package cn.donespeak.xiaoxiaole;

public class ScoreCalculator {

	public static int cal(int num) {
		if(num >= 5) {
			return 10;
		} else if (num == 4) {
			return 3;
		} else if (num == 3) {
			return 1;
		} else {
			return 0;
		}
	}
}
