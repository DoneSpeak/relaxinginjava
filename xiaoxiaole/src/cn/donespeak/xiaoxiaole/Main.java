package cn.donespeak.xiaoxiaole;

import java.util.Arrays;
import java.util.List;

public class Main {

	public static void main(String[] args) {
//		manualPlay();
		autoPlay();
	}
	
	private static void manualPlay() {
		GamePlayer gamePlayer = new GamePlayer(10);
		gamePlayer.manualPlay();
	}
	
	private static void autoPlay() {
		GamePlayer gamePlayer = new GamePlayer(1);
		List<TipsPoint> tips = gamePlayer.autoPlay();
//		
		System.out.println();
		System.out.println("=========== TEST TIPS =============");
		GamePlayer gamePlayer2 = new GamePlayer(10);
		gamePlayer2.autoPlay(tips);
	}
}