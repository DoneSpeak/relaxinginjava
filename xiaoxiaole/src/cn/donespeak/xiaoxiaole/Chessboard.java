package cn.donespeak.xiaoxiaole;

public class Chessboard {
	private int[][] chessboard;
	private int typeNum;
	
	public Chessboard() {
		
	}
	
	public Chessboard(int[][] chessboard, int typeNum) {
		this.chessboard = chessboard;
		this.typeNum = typeNum;
	}
	
	public int[][] getChessboard() {
		return chessboard;
	}
	public void setChessboard(int[][] chessboard) {
		this.chessboard = chessboard;
	}
	public int getTypeNum() {
		return typeNum;
	}
	public void setTypeNum(int typeNum) {
		this.typeNum = typeNum;
	}
}
