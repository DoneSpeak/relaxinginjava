package cn.donespeak.xiaoxiaole;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class GamePlayer {
	
	private XiaoXiaoLe xiaoXiaoLe;
	private int stepsLeft;
	
	public GamePlayer(int stepsLeft) {
		initXiaoXiaoLe();
		this.stepsLeft = stepsLeft;
	}
	
	public void initXiaoXiaoLe() {
		Chessboard chessboard = getAChessboard();
		xiaoXiaoLe = new XiaoXiaoLe(chessboard.getChessboard(), chessboard.getTypeNum());
	}
	
	private Chessboard getAChessboard() {
		List<int[][]> chessboards = new ArrayList<>();
		chessboards.add(new int[][] {
				{0, 0, 1, 0, 0},
				{3, 3, 1, 0, 4},
				{1, 1, 2, 1, 1},
				{2, 2, 3, 2, 2},
				{1, 1, 2, 1, 1},
				{1, 1, 2, 1, 1}
			});
		chessboards.add(new int[][] {
				{0, 0, 1, 0, 0},
				{3, 3, 1, 0, 4},
				{1, 1, 2, 1, 1},
				{2, 2, 1, 2, 2},
				{1, 1, 2, 1, 1},
				{1, 1, 2, 1, 1}
			});
		int typeNum = 4;
		return new Chessboard(chessboards.get(1), typeNum);
	}
	
	public List<TipsPoint> autoPlay() {
		xiaoXiaoLe.SHOW_PROCESSING = false;
		xiaoXiaoLe.SHOW_REMOVED_MARKS = false;
		xiaoXiaoLe.DEBUG = false;
		
		AutoPlayer autoPlay = new AutoPlayer(xiaoXiaoLe, stepsLeft);
		xiaoXiaoLe.showStatus();
		autoPlay.start();
		int maxScore = autoPlay.getMaxScore();
		xiaoXiaoLe.showStatus();
		System.out.println("Max Score is: " + maxScore);
		System.out.println("Tips: ");
		for(TipsPoint p: autoPlay.getTips()) {
			System.out.println(p);
		}
		return autoPlay.getTips();
	}

	public void autoPlay(List<TipsPoint> tips) {
		xiaoXiaoLe.SHOW_PROCESSING = true;
		xiaoXiaoLe.SHOW_REMOVED_MARKS = true;
		xiaoXiaoLe.DEBUG = false;
		for(TipsPoint t: tips) {
			int curScore = exchange(t);
			if(curScore == 0) {
				return;
			}
			xiaoXiaoLe.show();
			System.out.println();
		}
	}
	
	public void manualPlay() {
		showStartTip();
		Scanner scanner = new Scanner(System.in);

		xiaoXiaoLe.SHOW_PROCESSING = true;
		xiaoXiaoLe.SHOW_REMOVED_MARKS = true;
		xiaoXiaoLe.DEBUG = false;
		
		xiaoXiaoLe.show();
		System.out.println("Remain Steps: " + stepsLeft);
		System.out.println("=======================\n");
		
		while(stepsLeft > 0) {
			System.out.print("input row(-1 will output tips):");
			int row = scanner.nextInt();
			if(row < 0) {
				xiaoXiaoLe.getRemainder().showTips();
				continue;
			}
			System.out.print("input col:");
			int col = scanner.nextInt();
			
			System.out.print("input orient("
					+ "UP:" + ArrowE.UP.getValue() + ", "
					+ "RIGHT:" + ArrowE.RIGHT.getValue() + ", "
					+ "DOWN:" + ArrowE.DOWN.getValue() + ", "
					+ "LEFT:" + ArrowE.LEFT.getValue() 
					+ "):");
			
			int orient = scanner.nextInt();
			
			exchange(row, col, orient);
			stepsLeft --;
			xiaoXiaoLe.show();
			System.out.println("Remain Steps:\t" + stepsLeft);
			System.out.println("=======================\n");
			if(!xiaoXiaoLe.canContinue()) {
				System.out.println("===== Game Win =====");
				break;
			}
		}
		scanner.close();
	}
	
	private void showStartTip() {
		// TODO Auto-generated method stub
		
	}
	public int exchange(TipsPoint tips) {
		return exchange(tips.getRow(), tips.getCol(), tips.getArrow().getValue());
	}
	public int exchange(int row, int col, int orient) {
		ArrowE arrow = ArrowE.getE(orient);
		if(arrow == null) {
			System.out.println("The orient " + orient + " dosen't exist.");
			return 0;
		}
		try {
			return xiaoXiaoLe.exchangeWith(row, col, arrow);
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return 0;
		}
	}
}
