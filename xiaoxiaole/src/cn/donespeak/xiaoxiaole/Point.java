package cn.donespeak.xiaoxiaole;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public class Point implements Comparable<Point> {

	private int row;
	private int col;
	
	public Point() {
		this(0, 0);
	}
	
	public Point(int row, int col) {
		this.row = row;
		this.col = col;
	}
	
	public int getRow() {
		return row;
	}
	public void setRow(int row) {
		this.row = row;
	}
	public int getCol() {
		return col;
	}
	public void setCol(int col) {
		this.col = col;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + col;
		result = prime * result + row;
		return result;
	}
	
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Point other = (Point) obj;
		if (col != other.col)
			return false;
		if (row != other.row)
			return false;
		return true;
	}

	@Override
	public int compareTo(Point p) {
		// 按照行升序，再按照列升序
		if(p == null) {
			return 1;
		}
		int result = Integer.compare(row, p.getRow());
		return result == 0? Integer.compare(col, p.getCol()): result;
	}
	
	@Override
	public String toString() {
		return "(" + row + ", " + col + ")";
	}

	public static void main(String[] args) {
		List<Point> ps = Arrays.asList(new Point(1, 12), new Point(2, 3), 
				new Point(1, 5), new Point(2, 1));
		Collections.sort(ps);
		System.out.println(ps);
	}
	
}
